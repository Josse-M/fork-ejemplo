FROM node

WORKDIR /app

COPY package*.json ./

RUN npm ci

COPY . ./


ENV NODE_ENV=''
ENV PORT=''
ENV DB_USER=''
ENV DB_HOST=''
ENV DB_NAME=''
ENV DB_PASSWORD=''
ENV DB_PORT=''

EXPOSE 3000

CMD ["npm", "start"]
